Traffic Control Coding Standard
===================================

This repository contains settings for Traffic Control coding style.
Based on [Yii2 coding style](https://github.com/yiisoft/yii2-coding-standards)

PHP_Codesniffer
---------------

[PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer) coding standard, rule set
and sniff token parsing classes for Traffic Control.

### Using code style

After CodeSniffer is installed you can launch it with custom code style using the following syntax:

```
$ ./vendor/bin/phpcs --extensions=php --standard=TC .
```

### Useful links

* [Configuration options](http://pear.php.net/manual/en/package.php.php-codesniffer.config-options.php)
* [Manual and guide](https://github.com/squizlabs/PHP_CodeSniffer/wiki)
* [GitHub repository](https://github.com/squizlabs/PHP_CodeSniffer)
